class ValidationError(Exception):
    pass


class ParseError(Exception):
    pass
