import json
import unittest
import re
import requests
from itertools import permutations

from encoder import Encoder
from decoder import Decoder
from error_messages import ERR_EMPTY, ERR_LOAD
from exceptions import ParseError, ValidationError


def get_valid_raw_text():
    return (
            'This is a long looong test sentence,\n'
            'with some big (biiiiig) words!')


def get_valid_encoded_text():
    return (
            '\n—weird—\n'
            'Tihs is a lnog loonog tset sntceene,\n'
            'wtih smoe big (biiiiig) wdros!'
            '\n—weird—\n'
            'long looong sentence some test This with words')


def get_invalid_encoded_text():
    return (
            '\n—weir—\n'
            'Tihs is a lnog loonog tset sntceene,\n'
            'wtih smoe big (biiiiig) wdros!'
            '\n—weird—\n'
            'long looong sentence some test This with words')


class TestEncoder(unittest.TestCase):
    def get_header(self):
        return '\n—weird—\n'

    def test_empty_text(self):
        passed = False
        try:
            Encoder().encode('')
        except ValidationError:
            passed = True
        self.assertTrue(passed)

    def test_non_string_text(self):
        passed = False
        try:
            Encoder().encode(15)
        except ValidationError:
            passed = True
        self.assertTrue(passed)

    def test_single_word_text(self):
        word = 'hello'
        encoded_word = ''
        possible_encoded_word_list = [
                                'hello', 'hlelo',
                                'hlleo', 'hlelo', 'hlleo'
                                ]
        encoded_text_result = Encoder().encode(word)

        """
        loop for the correct encoded option of all possibles
        """
        passed = False
        for _encoded_word in possible_encoded_word_list:
            if _encoded_word in encoded_text_result:
                encoded_word = _encoded_word
                passed = True
        self.assertTrue(passed)

        """
        get weird text and dictionary
        """
        _, encoded_text, dictionary = \
            encoded_text_result.split(self.get_header())
        """
        test partly encoder method result
        """
        self.assertEqual(encoded_word, encoded_text)
        self.assertEqual(word, dictionary)

        """
        test encoder method result
        """
        _encoded_text_result = (self.get_header() + encoded_word +
                                self.get_header() + word)
        self.assertEqual(encoded_text_result, _encoded_text_result)

    def _test_multi_word_text(self, multi_word_text):
        tokenize_re = re.compile(r'(\w+)', re.U)
        tokenize_multi_word_list = tokenize_re.split(multi_word_text)

        generated_dictionary_list = []
        possible_encoded_multi_word_dict = {}

        encoded_multi_word_list = [''] * len(tokenize_multi_word_list)

        """
        generate possible encoded words from multi word text
        """
        for word in tokenize_multi_word_list:
            word = word.strip()
            for encoded_word in permutations(word[1:-1]):
                possible_encoded_word_list = []
                encoded_word_string = ''.join(encoded_word)
                if encoded_word_string != word[1:-1]:
                    """
                    if word exists in dict
                    """
                    if possible_encoded_multi_word_dict.get(word):
                        possible_encoded_word_list = \
                            possible_encoded_multi_word_dict.get(word)
                    else:
                        generated_dictionary_list.append(word)
                    possible_encoded_word_list.append(
                                                    word[0] +
                                                    encoded_word_string +
                                                    word[-1])
                    possible_encoded_multi_word_dict[word] = \
                        possible_encoded_word_list
        encoded_text_result = Encoder().encode(multi_word_text)
        tokenize_encoded_text_result = tokenize_re.split(encoded_text_result)
        """
        check which of generated possible endoded words is the genereted by Encoder one
        """
        for word_id in range(len(tokenize_multi_word_list)):
            possible_encoded_word_list = \
                possible_encoded_multi_word_dict.get(tokenize_multi_word_list[word_id])
            if possible_encoded_word_list is not None:
                passed = False
                temp = ''
                for possible_encoded_word in possible_encoded_word_list:
                    temp = possible_encoded_word
                    if possible_encoded_word in tokenize_encoded_text_result:
                        encoded_multi_word_list[word_id] = possible_encoded_word
                        passed = True
                self.assertTrue(passed)
            else:
                """
                dictionary do not contain that word -> word didn't change
                """
                not_changed_word = tokenize_multi_word_list[word_id]
                encoded_multi_word_list[word_id] = not_changed_word

        """
        sort generated dictionary
        """
        generated_dictionary_list.sort(key=str.casefold)

        """
        get weird text and dictionary
        """
        _, encoded_text, dictionary = \
            encoded_text_result.split(self.get_header())
        """
        test partly encoder method result
        """
        self.assertEqual(''.join(encoded_multi_word_list), encoded_text)
        self.assertEqual(' '.join(generated_dictionary_list), dictionary)

        """
        test encoder method result
        """
        _encoded_text_result = (self.get_header() +
                                ''.join(encoded_multi_word_list) +
                                self.get_header() +
                                ' '.join(generated_dictionary_list))
        self.assertEqual(encoded_text_result, _encoded_text_result)

    def test_multi_word_text(self):
        multi_word_string = 'like a shine from the moon-light shadow'
        self._test_multi_word_text(multi_word_string)

        multi_word_string = get_valid_raw_text()
        self._test_multi_word_text(multi_word_string)

        # test may take some time
        # multi_word_string = '''Quisque eu gravida mea, sed sollicitudin \
        # magna. Aliquam venenatis mattis. Vestibulum purus dui, vehicula \
        # non consectetur sed, maximus vel sapien. Nullam ante massa, rutrum \
        # sit amet nunc in, placerat aliquet neque. Mauris orci turpis, \
        # tempor ut lorem non, ullamcorper tempus velit. Donec eu elit nibh.'''
        # self._test_multi_word_text(multi_word_string)


class TestDecoder(unittest.TestCase):
    def test_load_valid_text(self):
        encoded_text = get_valid_encoded_text()

        expected_weird_text = (
            'Tihs is a lnog loonog tset sntceene,\n'
            'wtih smoe big (biiiiig) wdros!')
        expected_dictionary = 'long looong sentence some test This with words'

        weird_text, dictionary = Decoder()._load_text(encoded_text)

        self.assertEqual(expected_weird_text, weird_text)
        self.assertEqual(expected_dictionary, dictionary)

    def test_load_invalid_test(self):
        encoded_text = get_invalid_encoded_text()
        passed = False
        try:
            weird_text, dictionary = Decoder()._load_text(encoded_text)
        except ParseError:
            passed = True
        self.assertTrue(passed)

        encoded_text = (
            'Tihs is a lnog loonog tset sntceene,\n'
            'wtih smoe big (biiiiig) wdros!'
            '\n—weird—\n'
            'long looong sentence some test This with words')
        passed = False
        try:
            weird_text, dictionary = Decoder()._load_text(encoded_text)
        except ParseError:
            passed = True
        self.assertTrue(passed)

        encoded_text = (
            'Tihs is a lnog loonog tset sntceene,\n'
            'wtih smoe big (biiiiig) wdros!'
            'long looong sentence some test This with words')
        passed = False
        try:
            weird_text, dictionary = Decoder()._load_text(encoded_text)
        except ParseError:
            passed = True
        self.assertTrue(passed)

    def test_match_word_candidates(self):
        word = 'tset'
        dictionary = 'long looong sentence some test This with words'.split(' ')
        expected_result = ['test']
        result = Decoder()._match_word_candidates(word, dictionary)
        self.assertEqual(expected_result, result)

        word = 'sntceene'
        dictionary = 'long looong sentence some test This with words'.split(' ')
        expected_result = ['sentence']
        result = Decoder()._match_word_candidates(word, dictionary)
        self.assertEqual(expected_result, result)

        word = 'sntceene'
        dictionary = 'long looong some test This with words'.split(' ')
        expected_result = []
        result = Decoder()._match_word_candidates(word, dictionary)
        self.assertEqual(expected_result, result)

    def test_decode(self):
        encoded_text = get_valid_encoded_text()
        expected_decoded_text = get_valid_raw_text()
        decoded_text = Decoder().decode(encoded_text)
        self.assertEqual(expected_decoded_text, decoded_text)

        encoded_text = (
            '\n—weird—\n'
            'The sun htis her yloelw hsoue.'
            ' It is amslot lkie a sgin form God.'
            '\n—weird—\n'
            'almost from hits house like sign yellow')
        expected_decoded_text = (
            'The sun hits her yellow house.'
            ' It is almost like a sign from God.')
        decoded_text = Decoder().decode(encoded_text)
        self.assertEqual(expected_decoded_text, decoded_text)


class TestEncoderDecoder(unittest.TestCase):
    def test_encode_and_decode_text(self):
        text = get_valid_raw_text()
        encoded_text = Encoder().encode(text)
        decoded_text = Decoder().decode(encoded_text)
        self.assertEqual(decoded_text, text)

        text = (
            'Madzia is beautiful '
            'Beautiful enough for me '
            'The sun hits her yellow house '
            'It’s almost like a sign from God.')
        encoded_text = Encoder().encode(text)
        decoded_text = Decoder().decode(encoded_text)
        self.assertEqual(decoded_text, text)


class TestAPI(unittest.TestCase):
    def test_encoder_endpoint_valid_text(self):
        text = get_valid_raw_text()
        r = requests.post(
                        "http://localhost:5000/v1/encode",
                        data=json.dumps({'text': text}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertTrue(len(response_dict['text']) > 0)
        self.assertEqual(response_dict.get('message'), None)

    def test_decoder_endpoint_valid_text(self):
        text = get_valid_raw_text()
        encoded_text = get_valid_encoded_text()

        r = requests.post(
                        "http://localhost:5000/v1/decode",
                        data=json.dumps({'text': encoded_text}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(response_dict['text'], text)
        self.assertEqual(response_dict.get('message'), None)

    def test_encoder_invalid_text(self):
        text = ' '
        r = requests.post(
                        "http://localhost:5000/v1/encode",
                        data=json.dumps({'text': text}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(response_dict.get('text'),  None)
        self.assertEqual(ERR_EMPTY, response_dict['message'])

    def test_decoder_endpoint_invalid_text(self):
        encoded_text = get_invalid_encoded_text()

        r = requests.post(
                        "http://localhost:5000/v1/decode",
                        data=json.dumps({'text': encoded_text}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(response_dict.get('text'), None)
        self.assertEqual(ERR_LOAD, response_dict.get('message'))

    def test_encode_and_decove_valid_text(self):
        text = get_valid_raw_text()
        r = requests.post(
                        "http://localhost:5000/v1/encode",
                        data=json.dumps({'text': text}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertTrue(len(response_dict['text']) > 0)
        self.assertEqual(response_dict.get('message'), None)

        r = requests.post(
                        "http://localhost:5000/v1/decode",
                        data=json.dumps({'text': response_dict['text']}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(response_dict['text'], text)
        self.assertEqual(response_dict.get('message'), None)

    def test_encode_and_decove_valid_text_code_review(self):
        text = "amamab atatab"
        r = requests.post(
                        "http://localhost:5000/v1/encode",
                        data=json.dumps({'text': text}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertTrue(len(response_dict['text']) > 0)
        self.assertEqual(response_dict.get('message'), None)

        r = requests.post(
                        "http://localhost:5000/v1/decode",
                        data=json.dumps({'text': response_dict['text']}))
        response_dict = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(response_dict['text'], text)
        self.assertEqual(response_dict.get('message'), None)


if __name__ == '__main__':
    unittest.main()
