import random
import re

from error_messages import ERR_EMPTY, ERR_TYPE
from exceptions import ValidationError


class Encoder:
    def __init__(self):
        self.encoded_header = '\n—weird—\n'

    def _validate_text(self, text):
        """
        function validate passed text parameter type and lenght
        raise Exception if text is not valid
        """
        if not isinstance(text, str):
            err_msg = ERR_TYPE
            raise ValidationError(err_msg)

        if len(text.strip()) < 1:
            err_msg = ERR_EMPTY
            raise ValidationError(err_msg)

    def _encode_word(self, word):
        """
        function encode single word
        if passed word can be shuffled returns True value and encoded word
        else return False and origin word
        return Boolean, String
        """
        self._validate_text(word)

        shuffled = False
        first_char, middle_chars, last_char = word[0], word[1:-1], word[-1]
        shuffle_middle_chars = middle_chars

        """
        check if word can be shuffled differently
        and shuffle until generated word is different than origin
        """
        if middle_chars != middle_chars[0] * len(middle_chars):
            while shuffle_middle_chars == middle_chars:
                shuffle_middle_chars = \
                    ''.join(random.sample(middle_chars, len(middle_chars)))
            shuffled = True

        return shuffled, first_char + shuffle_middle_chars + last_char

    def encode(self, text):
        """
        function encode passed text
        returns encoded text and dictionary for the text
        return String
        """

        """
        validate text
        """
        self._validate_text(text)

        """
        variables to store data
        """
        encoded_word_list = []
        dictionary_list = []

        """
        split string text into list
        """
        tokenize_re = re.compile(r'(\w+)', re.U)
        words_list = tokenize_re.split(text)

        for word in words_list:
            """
            encode only words that contains more than 3 characters
            shorter words will be the same after encode
            example osx -encoder-> osx (the same)
            
            shorter words push to encoded words list
            """
            if len(word.strip()) <= 3:
                encoded_word_list.append(word)
            else:
                shuffled, encoded_word = self._encode_word(word)
                """
                if word was shuffled with success
                push origin word to dictionary list
                """
                if shuffled:
                    dictionary_list.append(word)
                """
                push encoded word to list
                """
                encoded_word_list.append(encoded_word)

        encoded_text = self.encoded_header +\
            ''.join(encoded_word_list) + self.encoded_header

        """
        sort alphabetically list caseless matching
        """
        dictionary_list.sort(key=str.casefold)

        return encoded_text + ' '.join(dictionary_list)
