import re

from error_messages import ERR_LOAD
from exceptions import ParseError


class Decoder:
    def __init__(self):
        self.encoded_header = '\n—weird—\n'

    def _load_text(self, text):
        """
        function loads encoded text
        split data by self.encoded_header separator
        if encoded text is valid returns encoded text and dictionary
        return str, str
        """
        try:
            _, encoded_text, dictionary = text.split(self.encoded_header)
        except:
            raise ParseError(ERR_LOAD)
        return encoded_text, dictionary

    def _match_word_candidates(self, word, dictionary_list):
        """
        function matches encoded words with words from dictionary
        return list of candidates words
        return list[str]
        """
        candidates_list = []

        for w in dictionary_list:
            """
            origin and encoded word have the same length
            """
            if len(word) == len(w):
                """
                origin and encoded words have the same first and last char
                """
                if word[0] == w[0] and word[-1] == w[-1]:
                    """
                    origin and encoded words contains with the same chars
                    """
                    if ''.join(sorted(word[1:-1])) == ''.join(sorted(w[1:-1])):
                        candidates_list.append(w)
        return candidates_list

    def decode(self, text):
        """
        function decodes encoded text
        return encoded text
        return str
        """
        decoded_word_list = []

        encoded_text, dictionary = self._load_text(text)

        """
        split string into list
        """
        tokenize_re = re.compile(r'(\w+)', re.U)
        encoded_words_list = tokenize_re.split(encoded_text)

        """
        split string into list
        """
        dictionary_list = dictionary.split(' ')

        for word in encoded_words_list:
            """
            words that contains less than 3 characters
            are the same before and after encoding
            """
            if len(word.strip()) <= 3:
                decoded_word_list.append(word)
            else:
                """
                look for encoded words candidates
                """
                matched_words = \
                    self._match_word_candidates(word, dictionary_list)
                """
                if any candidates found get first as the best matching
                (may be more than one candidate)

                if there isn't any candidate the word was not changed
                encrypted and raw words are the same
                """
                if len(matched_words) > 0:
                    decoded_word_list.append(matched_words[0])
                else:
                    decoded_word_list.append(word)

        return ''.join(decoded_word_list)
