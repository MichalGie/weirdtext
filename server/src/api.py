from flask import Flask, request
from flask_restful import reqparse, Api, Resource
from flask_restful.utils import cors

from decoder import Decoder
from encoder import Encoder

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('text', type=str)


class EncoderEndpoint(Resource):
    """
    Encoder
    encode POST text
    """
    @cors.crossdomain(origin='*')
    def post(self):
        request.get_json(force=True)
        args = parser.parse_args()
        text = args['text']
        try:
            encoded_text = Encoder().encode(text)
        except Exception as e:
            return {'message': str(e)}, 400
        return {'text': encoded_text}, 201


class DecoderEndpoint(Resource):
    """
    Decoder
    decode POST text
    """
    @cors.crossdomain(origin='*')
    def post(self):
        request.get_json(force=True)
        args = parser.parse_args()
        text = args['text']
        try:
            decoded_text = Decoder().decode(text)
        except Exception as e:
            return {'message': str(e)}, 400
        return {'text': decoded_text}, 201


api.add_resource(EncoderEndpoint, '/v1/encode')
api.add_resource(DecoderEndpoint, '/v1/decode')


if __name__ == '__main__':
    app.run(debug=True)
