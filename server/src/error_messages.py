"""
Encoder error messages
"""
ERR_EMPTY = 'ERROR passed text must be not empty'
ERR_TYPE = 'ERROR passed text must be string type'

"""
Decoder error messages
"""
ERR_LOAD = 'ERROR text is not valid, can\'t be parsed properly'
