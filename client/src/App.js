import React, { Component } from 'react';
import './App.css';

import APIConsumer from './components/APIConsumer';

class App extends Component {

  render() {
    return (
      <div className="app">
        <div className="app-header">Weirdtext encoder and decoder</div>
        <div className="api-consumer-wrapper">
          <APIConsumer
                name = 'Encoder'
                api_url="http://localhost:5000/v1/encode"
                defaultValue="This is a long looong test sentence,\nwith some big (biiiiig) words!"/>
           <APIConsumer
                name = 'Decoder'
                api_url="http://localhost:5000/v1/decode"/>
        </div>
      </div>
    )
  }
}

export default App;
