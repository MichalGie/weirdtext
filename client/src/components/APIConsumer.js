import React, { Component } from 'react';

class APIConsumer extends Component {
    constructor(props){
        super(props)
        this.state = {
            text: '',
            response_status: '',
            response_text: '',
            response_message: '',
        }
    }

    submit_request = () =>{
        this.setState({
                text: this.refs.textarea.value.replace(/\\n/g, "\n"),
            },
            this.request_api
        );
    };

    request_api = async() =>{
        const {api_url} = this.props
        fetch(api_url, {
            method: 'POST',
            body: JSON.stringify({
                text: this.state.text,
            }),
        })
        .then(response => {
            this.setState({
                response_status: response.status
            })
            return response.json()
        })
        .then(response => {
            if(this.state.response_status !== 201){
                this.setState({
                    response_message: response.message
                })
            }else{
                this.setState({
                    response_text: response.text.replace(/\n/g, '\\n')
                })
            }
        })
        .catch(error => {
            this.setState({
                response_status: 404,
                response_message: error.message,
            })
        });
    }

  render() {
    return (
    <div className="api-consumer-component-container">
        <div className="request-error-message">
            <div>{this.state.response_status !== 201 ? this.state.response_message : ''}</div>
        </div>
        <div className="container-name">{this.props.name}</div>
        <div className="api-request-container">
            <div className="textarea-container">
                <textarea
                    ref='textarea'
                    rows="4"
                    cols="50"
                    defaultValue={this.props.defaultValue ? this.props.defaultValue : ''}/>
            </div>
            <div className="request-submit-container">
                <div
                    className="request-submit"
                    onClick={this.submit_request}>
                    &#8594;
                </div>
            </div>
            <div className="response-container">
                <div>
                    {this.state.response_status === 201 && this.state.response_text ? this.state.response_text : ''}
                </div>
            </div>
        </div>
      </div>
    )
  }
}

export default APIConsumer;
