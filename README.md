# Project

Weirdtext encoder and decoder


## Live

Project is deployed to Heroku
Client url
```
https://weirdtext19.herokuapp.com/
```

Server endpoints
```
https://weirdtext19api.herokuapp.com/v1/encode
https://weirdtext19api.herokuapp.com/v1/decode
```


## Running project

client -> npm install -> npm start
server -> pip install -r requirements.txt -> python3 api.py


## API Reference

encode POST uri
```
v1/encode
```

decode POST uri
```
v1/decode
```


### Break down into end to end tests

to run server tests execute python3 tests.py


## Built With

* [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Flask-restful](https://flask-restful.readthedocs.io) - A microframework for Python


## Known bugs

* issue with text that contains more than one (valid to encode - more than 3 chars) the same words. (shuffle algorithm is not deterministic)

